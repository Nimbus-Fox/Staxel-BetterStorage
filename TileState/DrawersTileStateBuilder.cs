﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Plukit.Base;
using Staxel.Logic;
using Staxel.Tiles;
using Staxel.TileStates;

namespace NimbusFox.BetterStorage.TileState {
    public class DrawersTileStateBuilder : ITileStateBuilder {
        /// <summary>Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.</summary>
        public void Dispose() { }
        public void Load() { }
        public static string KindCode => "nimbusfox.betterStorage.tileState.drawers";
        public string Kind() {
            return KindCode;
        }

        public Entity Instance(Vector3I location, Tile tile, Universe universe) {
            return DrawersTileStateEntityBuilder.EntitySpawn(location, universe, tile);
        }
    }
}
