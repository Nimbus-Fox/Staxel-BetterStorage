﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Staxel.Docks;
using Staxel.Logic;
using Staxel.TileStates.Docks;

namespace NimbusFox.BetterStorage.TileState {
    public class DrawersTileStateLogic : DockTileStateEntityLogic {
        public DrawersTileStateLogic(Entity entity) : base(entity) { }

        protected override void AddSite(DockSiteConfiguration config) {
            _dockSites.Add(new DockSite(Entity, new DockSiteId(Entity.Id, _dockSites.Count), config));
        }
    }
}
