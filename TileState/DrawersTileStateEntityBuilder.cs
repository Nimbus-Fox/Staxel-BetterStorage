﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Plukit.Base;
using Staxel.Logic;
using Staxel.Tiles;
using Staxel.TileStates.Docks;

namespace NimbusFox.BetterStorage.TileState {
    public class DrawersTileStateEntityBuilder : DockTileStateEntityBuilder, IEntityLogicBuilder, IEntityPainterBuilder {
        public new static string KindCode => "nimbusfox.betterStorage.tileState.drawers";

        string IEntityPainterBuilder.Kind => KindCode;
        string IEntityLogicBuilder.Kind => KindCode;

        EntityLogic IEntityLogicBuilder.Instance(Entity entity, bool server) {
            return new DrawersTileStateLogic(entity);
        }

        public static Entity EntitySpawn(Vector3I position, EntityUniverseFacade universe, Tile tile) {
            var entity = new Entity(universe.AllocateNewEntityId(), false, KindCode, true);

            var blob = BlobAllocator.Blob(true);

            blob.SetString("kind", KindCode);
            blob.FetchBlob("location").SetVector3I(position);
            blob.SetLong("variant", tile.Variant());
            blob.SetString("tile", tile.Configuration.Code);
            blob.FetchBlob("velocity").SetVector3D(Vector3D.Zero);

            entity.Construct(blob, universe);

            universe.AddEntity(entity);

            Blob.Deallocate(ref blob);

            return entity;
        }
    }
}
