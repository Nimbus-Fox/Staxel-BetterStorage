﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Staxel.Docks;
using Staxel.Logic;
using Staxel.Player;
using Staxel.TileStates.Docks;

namespace NimbusFox.BetterStorage.Docks {
    public class DrawerDockSite : DockSite {
        private int _limit = 5000;
        public DrawerDockSite(Entity entity, DockSiteId id, DockSiteConfiguration dockSiteConfig) : base(entity, id,
            dockSiteConfig) {
        }

        public override bool TryDock(Entity entity, EntityUniverseFacade facade, ItemStack stack, uint rotation) {
            if (DockedItem.Stack.IsNull() || DockedItem.Stack.Count == 0) {
                DockedItem.Stack = stack;
                entity.Inventory.RemoveItem(stack);
                return true;
            }
            
            if (DockedItem.Stack.Item.GetItemCode() == stack.Item.GetItemCode()) {
                var amount = stack.Count;
                var diff = (_limit - DockedItem.Stack.Count);

                if (diff - amount < 0) {
                    amount -= diff;
                }

                DockedItem.Stack.Count += amount;

                entity.Inventory.RemoveItem(new ItemStack(stack.Item, amount));

                return true;
            }

            return false;
        }

        public override bool TryUndock(PlayerEntityLogic player, EntityUniverseFacade facade, int quantity, int mult) {
            return base.TryUndock(player, facade, quantity, mult);
        }

        public override int CanDock(ItemStack stack) {
            if (DockedItem.Stack.IsNull() || DockedItem.Stack.Count == 0) {
                DockedItem.Stack = stack;
                return stack.Count;
            }

            if (DockedItem.Stack.Item.GetItemCode() == stack.Item.GetItemCode() && DockedItem.Stack.Count != _limit) {
                var amount = stack.Count;
                var diff = (_limit - DockedItem.Stack.Count);

                if (diff - amount < 0) {
                    amount -= diff;
                }

                DockedItem.Stack.Count += amount;

                return amount;
            }

            return 0;
        }
    }
}
